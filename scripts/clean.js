#!/usr/bin/env zx

(async () => {
    const { globby, spinner, fs, chalk } = await import("zx");
    await spinner('Running clean process...', async () => {
        const paths = await globby(["**/dist", "**/node_modules"], { onlyDirectories: true});

        for (const path of paths) {
            await fs.rm(path, { recursive: true, force: true });
        }
    });

    console.log(chalk.green("Cleaned all dist and node_modules folders"));
})();
