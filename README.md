<center>
<h1>vue3-typescript-monorepo-starter</h1>
</center>

## Stack
- 🐉 **lerna** for workspace management and streaming command through multiple packages
- 🪄 **eslint** for linting and formatting
- 💅 **stylelint** for scss and css linting and formatting
- 🛠️ **tsc** and **vue-tsc** for typescript support and type-checking
- ⚡️ **vite** for lightning fast development
- 🖼️ **vue3** for frontend framework
- 🍍 **pinia** for state management
- 📖 **histoire** for ui component library development 

## Project Structure
```
.
├── packages
│   ├── app      <- main application
│   ├── common   <- shared code
│   ├── histoire <- shared ui components
│   └── scripts  <- common scripts
```

## Project commands
| Command | Description |
|---------|-------------|
| `npm start` | Start development server |
| `npm run histoire` | Start histoire development preview app |
| `npm run build` | Build for development environnement |
| `npm run build-test` | Build for test/staging environnement |
| `npm run build-prod` | Build for production environnement |
| `npm run type-check` | Run type check against all packages |
| `npm run eslint` | Run eslint check against all packages |
| `npm run stylelint` | Run stylelint check against all packages |
| `npm run lint` | Run eslint, stylelint and type check against all packages |
| `npm run script:clean` | Remove every `**/dist` and `**/node_modules` folders |