/// <reference types="histoire" />

import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import { resolve } from 'path'

export default defineConfig({
  plugins: [vue()],
  clearScreen: false,
  resolve: {
    alias: {
      "@cogifor/common":  resolve(process.cwd(), "../common/src"),
    },
    dedupe: ['vue'],
  }
})
