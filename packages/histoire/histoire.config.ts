import { HstVue } from "@histoire/plugin-vue";
import { defineConfig } from "histoire";
import { resolve } from "path";

export default defineConfig({
  setupFile: "./src/setup.ts",
  plugins: [
    HstVue()
  ],
  vite: {
    resolve: {
      alias: {
        // eslint-disable-next-line @typescript-eslint/naming-convention
        "@cogifor/common": resolve(process.cwd(), "../common/src")
      }
    }
  }
});