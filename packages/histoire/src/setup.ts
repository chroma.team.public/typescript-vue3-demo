import "quasar/dist/quasar.css";

import { defineSetupVue3 } from "@histoire/plugin-vue";
import { Quasar } from "quasar";

import quasarConfig from "./quasar.config";

export const setupVue3 = defineSetupVue3(({ app }): void => {
  app.use(Quasar, quasarConfig);
});