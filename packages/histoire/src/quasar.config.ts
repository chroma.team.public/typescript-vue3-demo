import { QuasarPluginOptions } from "quasar";
import lang from "quasar/lang/fr";

const quasarConfig: Partial<QuasarPluginOptions> = {
  lang,
  config: {
    brand: {
      primary: "#005b8f",
      secondary: "#ff0",
      accent: "#0f0",
      dark: "#000",
      positive: "#0f0",
      negative: "#f00",
      info: "#00f",
      warning: "#ff0"
    }
  }
};

export default quasarConfig;