import { defineConfig, loadEnv } from 'vite'
import vue from '@vitejs/plugin-vue'
import { resolve } from 'path';

export default defineConfig(({ mode }) => {
  const env = loadEnv(mode, process.cwd());

  return {
    base: env.VITE_APP_BASE_PATH,
    plugins: [vue()],
    clearScreen: false,
    resolve: {
      alias: {
        "@cogifor/histoire": resolve(process.cwd(), "../histoire/src"),
        "@cogifor/common":  resolve(process.cwd(), "../common/src"),
        "@": resolve(process.cwd(), "./src")
      },
      dedupe: ["vue"]
    },
  }
});