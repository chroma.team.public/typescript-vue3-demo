import "quasar/dist/quasar.css";

import { createPinia } from "pinia";
import { Quasar } from "quasar";
import { createApp } from "vue";
import { createI18n } from "vue-i18n";

import en from "@cogifor/common/locales/en.json" assert { type: "json" };
import fr from "@cogifor/common/locales/fr.json" assert { type: "json" };

import App from "./App.vue";
import quasarConfig from "./quasar.config";

const app = createApp(App);

app.use(createPinia());

app.use(createI18n({
  legacy: false,
  locale: "fr",
  messages: {
    fr,
    en
  }
}));

app.use(Quasar, quasarConfig);

app.mount("#app");
